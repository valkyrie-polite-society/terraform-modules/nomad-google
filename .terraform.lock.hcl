# This file is maintained automatically by "terraform init".
# Manual edits may be lost in future updates.

provider "registry.terraform.io/adriennecohea/nomadutility" {
  version     = "0.0.14"
  constraints = ">= 0.0.14"
  hashes = [
    "h1:2FLpOL7JeaSu1s+P02LQZu3VgrvqjmHRPPb6Fodx/ZA=",
    "zh:27747a1da7905ed7711e8c9c962809a9fc054ef3571fdb21d4638fa56543d13c",
    "zh:a0596f0198fdfa5250de0646fb92532fe573f7357edb416da1b09fe8aa42f973",
    "zh:a07a1e759b24477d30ac3e85515c7ed805cb7a9aae9662b40cd25440f9153970",
    "zh:dfa00cfa874d922ad9b38e9ec0f54cd8c44d766e6fda575487e5fa0a780ddcd3",
  ]
}

provider "registry.terraform.io/hashicorp/google" {
  version     = "3.40.0"
  constraints = "~> 3.40.0"
  hashes = [
    "h1:09F9Q62aeWgIVEuAqA9WafkvSzHdvDUCpFni0X3GVyY=",
    "zh:1034d628bfb6c751abce884a2e7035abd2e05a2f944818ead2bc53bdf8f33914",
    "zh:4285e594dde25ec948b4e5a73f727769d429758a7b4e8b9bcb66fdb51c68a73f",
    "zh:83ed507eebd4c05a3bb2508956bba68ac1336aa3a7778256d131aa388aaa905d",
    "zh:98fba5b49bf6a686cabc4fe5d187372849936bab279672bb3de466cf28823416",
    "zh:9e08989bc04a5e3ca0ebc2aca634f1a873105465532f361e1d3382a5b11d4fa5",
    "zh:b781f2bec0f9d7375fbdb769ed151c2ddff33231c148609a18d45b74eb0f7371",
    "zh:bf1432de90f1a856c7d895498cc40944bc13f58af5045bb43724595259cd1d11",
    "zh:c38ec5aa03aed5dae49062c30691722fa49e0c5ac1e730c635c69e927b0c6845",
    "zh:d4eb990842a95fef449a4bf6aec625268fa963fd1ebc793060acd0e10512628a",
    "zh:daac70787ad80c760e24da74b7b0f4b2db9414b92347a1ce95aa3a5a3e1261d3",
  ]
}

provider "registry.terraform.io/hashicorp/random" {
  version = "3.0.0"
  hashes = [
    "h1:grDzxfnOdFXi90FRIIwP/ZrCzirJ/SfsGBe6cE0Shg4=",
    "zh:0fcb00ff8b87dcac1b0ee10831e47e0203a6c46aafd76cb140ba2bab81f02c6b",
    "zh:123c984c0e04bad910c421028d18aa2ca4af25a153264aef747521f4e7c36a17",
    "zh:287443bc6fd7fa9a4341dec235589293cbcc6e467a042ae225fd5d161e4e68dc",
    "zh:2c1be5596dd3cca4859466885eaedf0345c8e7628503872610629e275d71b0d2",
    "zh:684a2ef6f415287944a3d966c4c8cee82c20e393e096e2f7cdcb4b2528407f6b",
    "zh:7625ccbc6ff17c2d5360ff2af7f9261c3f213765642dcd84e84ae02a3768fd51",
    "zh:9a60811ab9e6a5bfa6352fbb943bb530acb6198282a49373283a8fa3aa2b43fc",
    "zh:c73e0eaeea6c65b1cf5098b101d51a2789b054201ce7986a6d206a9e2dacaefd",
    "zh:e8f9ed41ac83dbe407de9f0206ef1148204a0d51ba240318af801ffb3ee5f578",
    "zh:fbdd0684e62563d3ac33425b0ac9439d543a3942465f4b26582bcfabcb149515",
  ]
}

provider "registry.terraform.io/hashicorp/tls" {
  version = "3.0.0"
  hashes = [
    "h1:LtCEW5v1E5Eo49+kQOsKHRYf9Hc8ZR0jTpK+mXszPHs=",
    "zh:05eac573a1fe53227bcc6b01daf6ddf0b73456f97f56f316f1b3114a4771e175",
    "zh:09390dad764c76f0fd59cae4dad296e3e39487e06de3a4bc0df73916c6bb2f17",
    "zh:142d0bc4722ab088b7ca124b0eb44206b9d100f51035c162d50ef552e09813d0",
    "zh:2c391743dd20f43329c0d0d49dec7827970d788115593c0e32a57050c0a85337",
    "zh:525b12fc87369c0e6d347afe6c77668aebf56cfa078bb0f1f01cc2ee01ac7016",
    "zh:5583d81b7a05c6d49a4c445e1ee62e82facb07bb9204998a836b7b522a51db8d",
    "zh:925e3acc70e18ed1cd296d337fc3e0ca43ac6f5bf2e660f24de750c7754f91aa",
    "zh:a291457d25b207fd28fb4fad9209ebb591e25cfc507ca1cb0fb8b2e255be1969",
    "zh:bbf9e2718752aebfbd7c6b8e196eb2e52730b66befed2ea1954f9ff1c199295e",
    "zh:f4b333c467ae02c1a238ac57465fe66405f6e2a6cfeb4eded9bc321c5652a1bf",
  ]
}

provider "registry.terraform.io/hashicorp/vault" {
  version = "3.1.1"
  hashes = [
    "h1:zNh/4ZsBQPtDEDQpdGqscGoY7UCuGzZyD+GqZN+CFFk=",
    "zh:1e6f2da03e952a89bf0b977bc9d769328afef38daafd289322f777c9a3a98f15",
    "zh:33929385f37bfd28359a477d0f263c79726ae52ce8c9bbd8a46588ee5eead884",
    "zh:57557d0aae4b2473f3eb26cac7acdf1fd085b5f5c59ec513f6aa55ffd89abb25",
    "zh:6c6c23eebe613411f8a090fffaa4190303c28c297ea695e137e5a981369a9088",
    "zh:84e93a2e29d7de867a63935b9622242627904f2031c8d70485307c7dea2337ea",
    "zh:8de79c4c2d155d6c670854e3db6b03a746db1bc42f04c8700181a87e0882ec88",
    "zh:c0d89fa498faf848d32ddd1afa14691c4faf7880b3266b4a2d318cbfdda15173",
    "zh:cb1ec4b082c91208984f04a02a4fc6dea4cfcc33a5381f84721d2101135abac7",
    "zh:d040e6330d213d0c08426e42725eaa16a0f06f33911756553cca7eb5fa34aaeb",
    "zh:f0508f85e9e8bdebf996cdaf44d6c45b524d665d67de89dec5a7da04493a42b6",
    "zh:f83325d83c8e21a6cce47d269dbfa126aa154cf0d679358b3ef96e0929fcd76b",
  ]
}
