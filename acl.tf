resource "nomadutility_acl_bootstrap" "init" {
  address = "https://${google_compute_address.public[0].address}:4646"
  ca_file = pathexpand(var.ca_file)
}
