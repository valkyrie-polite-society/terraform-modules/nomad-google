resource "google_compute_address" "public" {
  count = var.cluster_size

  name   = "${var.cluster_name}-public-${count.index}"
  region = var.region
}

resource "google_compute_address" "private" {
  count        = var.cluster_size
  name         = "${var.cluster_name}-private-${count.index}"
  region       = var.region
  address_type = "INTERNAL"
}
