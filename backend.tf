terraform {
  backend "gcs" {
    bucket = "adrienne-devops-terraform-states"
    prefix = "gitlab.com/adriennes-spells/nomad-google"
  }
}
