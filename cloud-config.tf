module "cloud_config" {
  count  = var.cluster_size
  source = "git::https://gitlab.com/valkyrie-polite-society/terraform-modules/cloud-config.git?ref=6ef246f9d64e655fdf924d0468fa5a5bcabc550e"

  write_files = {
    # Nomad server configuration

    "/etc/nomad/ca.pem"  = data.terraform_remote_state.ca.outputs.root_ca_certificate_pem
    "/etc/nomad/tls.key" = tls_private_key.nomad[count.index].private_key_pem
    "/etc/nomad/tls.crt" = join("\n", [
      tls_locally_signed_cert.nomad[count.index].cert_pem,
      data.terraform_remote_state.ca.outputs.intermediate_ca_certificate_pem,
      data.terraform_remote_state.ca.outputs.root_ca_certificate_pem,
    ])

    "/etc/nomad/server.hcl" = templatefile("${path.module}/templates/nomad/server.hcl", {
      cluster_size = var.cluster_size
      gossip_key   = random_id.gossip.b64_std
    })

    # Consul agent configuration

    "/etc/consul/ca.pem"  = data.terraform_remote_state.ca.outputs.root_ca_certificate_pem
    "/etc/consul/tls.key" = tls_private_key.consul[count.index].private_key_pem
    "/etc/consul/tls.crt" = join("\n", [
      tls_locally_signed_cert.consul[count.index].cert_pem,
      data.terraform_remote_state.ca.outputs.intermediate_ca_certificate_pem,
      data.terraform_remote_state.ca.outputs.root_ca_certificate_pem,
    ])

    "/etc/consul/gossip.json" = jsonencode({
      encrypt = data.terraform_remote_state.consul.outputs.gossip_key
    })
    "/etc/consul/retry-join.json" = jsonencode({
      retry_join = ["provider=gce project_name=${var.project} tag_value=${var.consul_auto_join_tag}"]
    })
    "/etc/consul/datacenter.json" = jsonencode({
      datacenter = var.consul_datacenter
    })
    "/etc/consul/acl.hcl" = templatefile("${path.module}/templates/consul/acl.hcl", {
      consul_acl_token = data.terraform_remote_state.consul.outputs.root_token
    })

    "/etc/vault-agent/consul.hcl.tpl" = templatefile("${path.module}/templates/vault-agent/consul.hcl.tpl", {
      consul_creds_path = "consul/creds/nomad-server"
    })
    "/etc/vault-agent/vault.hcl.tpl" = templatefile("${path.module}/templates/vault-agent/vault.hcl.tpl", {
      address = "https://asgard-vault.adriennecohea.io:8200"
    })
    "/etc/vault-agent/config.hcl" = templatefile("${path.module}/templates/vault-agent/config.hcl", {
      address         = "https://asgard-vault.adriennecohea.io:8200"
      service_account = google_service_account.nomad.email
    })
  }
}
