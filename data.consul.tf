data "terraform_remote_state" "consul" {
  backend = "gcs"

  config = {
    bucket = "adrienne-devops-terraform-states"
    prefix = "gitlab.com/adriennes-spells/consul-google"
  }
}
