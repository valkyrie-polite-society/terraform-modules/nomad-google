resource "google_compute_instance_group" "nomad" {
  name      = var.cluster_name
  zone      = var.zone
  network   = data.google_compute_network.default.self_link
  instances = google_compute_instance.nomad.*.self_link
}
