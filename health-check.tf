resource "google_compute_region_health_check" "nomad" {
  name   = var.cluster_name
  region = var.region

  https_health_check {
    port         = 4646
    request_path = "/v1/agent/health"
  }
}
