resource "google_dns_record_set" "nomad" {
  name         = "${var.cluster_name}.${data.google_dns_managed_zone.domain.dns_name}"
  type         = "A"
  ttl          = 300
  managed_zone = data.google_dns_managed_zone.domain.name
  rrdatas      = [google_compute_forwarding_rule.nomad.ip_address]
}

data "google_dns_managed_zone" "domain" {
  name = var.dns_managed_zone
}

resource "google_compute_forwarding_rule" "nomad" {
  name       = "${var.cluster_name}-https"
  region     = var.region
  port_range = 4646
  target     = google_compute_target_pool.nomad.self_link
}

resource "google_compute_target_pool" "nomad" {
  name   = var.cluster_name
  region = var.region

  instances = [for i in range(var.cluster_size) : "${var.zone}/${var.cluster_name}-${i}"]

  health_checks = []
}
