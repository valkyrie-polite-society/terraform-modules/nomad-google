resource "google_compute_instance" "nomad" {
  count        = var.cluster_size
  name         = "${var.cluster_name}-${count.index}"
  machine_type = var.machine_type
  zone         = var.zone
  tags         = [var.cluster_name]

  boot_disk {
    initialize_params {
      image = data.google_compute_image.nomad.self_link
    }
  }

  attached_disk {
    source      = google_compute_disk.nomad[count.index].self_link
    device_name = "nomad"
  }

  network_interface {
    network    = data.google_compute_network.default.self_link
    network_ip = google_compute_address.private[count.index].address

    access_config {
      nat_ip = google_compute_address.public[count.index].address
    }
  }

  service_account {
    email  = google_service_account.nomad.email
    scopes = ["cloud-platform"]
  }

  scheduling {
    preemptible       = true
    automatic_restart = false
  }

  metadata = {
    "user-data" = module.cloud_config[count.index].yaml
  }
}
