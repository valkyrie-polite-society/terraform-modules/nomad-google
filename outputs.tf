output "acl_token" {
  value       = nomadutility_acl_bootstrap.init
  description = "Bootstrap ACL"
  sensitive   = true
}
