resource "google_service_account" "nomad" {
  account_id   = "${var.cluster_name}-nomad"
  display_name = "Nomad Service Account - ${var.cluster_name}"
}

resource "google_project_iam_member" "roles" {
  for_each = toset(["roles/compute.serviceAgent", "roles/compute.networkViewer"])
  project  = var.project
  role     = each.key
  member   = "serviceAccount:${google_service_account.nomad.email}"
}

moved {
  from = google_project_iam_member.service_agent
  to   = google_project_iam_member.roles["roles/compute.serviceAgent"]
}
