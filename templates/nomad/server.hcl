server {
  enabled          = true
  bootstrap_expect = ${cluster_size}
  encrypt          = "${gossip_key}"
}
