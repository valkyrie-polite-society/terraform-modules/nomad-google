vault {
  address = "${address}"
  ca_cert = "/etc/nomad/ca.pem"
}

auto_auth {
  method "gcp" {
    config = {
      role            = "nomad-server"
      type            = "iam"
      service_account = "${service_account}"
    }
  }
}

template {
  source      = "/etc/vault-agent/consul.hcl.tpl"
  destination = "/etc/nomad/consul.hcl"
}

template {
  source      = "/etc/vault-agent/vault.hcl.tpl"
  destination = "/etc/nomad/vault.hcl"
}
