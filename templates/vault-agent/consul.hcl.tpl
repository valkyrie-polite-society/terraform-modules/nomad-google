{{- with secret "${consul_creds_path}" -}}
consul {
  address   = "127.0.0.1:8501"
  ssl       = true
  ca_file   = "/etc/consul/ca.pem"
  cert_file = "/etc/consul/tls.crt"
  key_file  = "/etc/consul/tls.key"
  token     = "{{ .Data.token }}"
}
{{- end }}
