{{- with secret "auth/token/create/nomad-server" "orphan=true" -}}
vault {
  enabled = true
  address = "${address}"
  token   = "{{ .Auth.ClientToken }}"
  ca_file = "/etc/nomad/ca.pem"
}
{{- end }}
