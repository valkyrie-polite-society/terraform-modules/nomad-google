variable "project" {
  type = string
}

variable "zone" {
  type = string
}

variable "region" {
  type = string
}

variable "network" {
  type = string
}

variable "cluster_size" {
  default = 3

  validation {
    condition     = tonumber(var.cluster_size) == 3 || tonumber(var.cluster_size) == 5
    error_message = "The cluster must contain either 3 or 5 members."
  }
}

variable "cluster_name" {
  type = string
}

variable "ca_file" {
  type = string
}

variable "certificate_authority_remote_state_bucket" {
  type = string
}

variable "certificate_authority_remote_state_prefix" {
  type = string
}

variable "consul_datacenter" {
  type = string
}

variable "consul_auto_join_tag" {
  type = string
}

variable "machine_type" {
  type = string
}

variable "dns_managed_zone" {
  type = string
}

variable "tls_certificate_ttl_hours" {
  default = 24 * 365
}

variable "vault_address" {
  type = string
}
