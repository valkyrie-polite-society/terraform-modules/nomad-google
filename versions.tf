terraform {
  required_version = ">= 0.14.0"

  required_providers {
    google = {
      source  = "hashicorp/google"
      version = "~> 3.40.0"
    }

    nomadutility = {
      source  = "AdrienneCohea/nomadutility"
      version = ">= 0.0.14"
    }
  }
}
